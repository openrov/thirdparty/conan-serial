#include <string>
#include <iostream>
#include <cstdio>
#include <unistd.h>

#include <serial/serial.h>

void my_sleep(unsigned long milliseconds) 
{
    usleep(milliseconds*1000); // 100 ms
}

void enumerate_ports()
{
	auto devices_found  = serial::list_ports();
	auto iter           = devices_found.begin();

    std::cout << "Found " << std::to_string( devices_found.size() ) << " devices:" << std::endl;
	while( iter != devices_found.end() )
	{
		serial::PortInfo device = *iter++;
		printf( "(%s, %s, %s)\n", device.port.c_str(), device.description.c_str(), device.hardware_id.c_str() );
	}
}

int main(int argc, char **argv) 
{
    try 
    {
        enumerate_ports();
        return 0;
    } 
    catch(const std::exception &e ) 
    {
        std::cerr << "Unhandled Exception: " << e.what() << std::endl;
        return 1;
    }
}